from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

from project.apps.blog.models import Post, Comment, Like
from project.apps.blog.forms import CommentForm, LikeForm


class Home(generic.ListView):
    template_name = 'home.html'
    model = Post
    paginate_by = 10


class PostDetailView(generic.DetailView):
    template_name = 'post_detail.html'
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = CommentForm(initial={'post': self.object})
        context['like_form'] = LikeForm(initial={'post': self.object})
        return context


class CommentCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = None
    model = Comment
    form_class = CommentForm

    def get_success_url(self):
        return reverse('post_detail', args=[self.object.post_id])

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super().form_valid(form)

    def form_invalid(self, form):
        return HttpResponse('Форма не валидна', status=400)


class LikeView(LoginRequiredMixin, generic.CreateView):
    template_name = None
    model = Like
    form_class = LikeForm

    def get_success_url(self):
        return reverse('post_detail', args=[self.object.post_id])

    def form_valid(self, form):
        user = self.request.user
        post = form.cleaned_data.get('post')
        like = Like.objects.filter(user=user, post=post).last()
        if like is not None:
            self.object = like
            return HttpResponseRedirect(self.get_success_url())

        form.instance.user = user
        return super().form_valid(form)

    def form_invalid(self, form):
        return HttpResponse('Форма не валидна', status=400)
