from django import forms
from project.apps.blog.models import Comment, Like, Post


class CommentForm(forms.ModelForm):
    post = forms.ModelChoiceField(widget=forms.HiddenInput(), queryset=Post.objects.all())

    class Meta:
        model = Comment
        fields = ['text', 'post']


class LikeForm(forms.ModelForm):
    post = forms.ModelChoiceField(widget=forms.HiddenInput(), queryset=Post.objects.all())

    class Meta:
        model = Like
        fields = ('post', )
