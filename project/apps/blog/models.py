from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Post(models.Model):
    title = models.CharField('название', max_length=255)
    text = models.TextField('текст')
    created = models.DateTimeField('создана', auto_now_add=True)

    class Meta:
        verbose_name = 'пост'
        verbose_name_plural = 'посты'

    def __str__(self):
        return self.title

    def get_likes_count(self):
        return self.likes.count()

    def get_comments(self):
        return self.comments.all()


class Like(models.Model):
    post = models.ForeignKey(Post, verbose_name='пост', related_name='likes', on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name='пользователь', related_name='likes', on_delete=models.CASCADE)
    created = models.DateTimeField('поставлен', auto_now_add=True)

    class Meta:
        verbose_name = 'лайк'
        verbose_name_plural = 'лайки'
        unique_together = ('post', 'user')


class Comment(models.Model):
    user = models.ForeignKey(User, verbose_name='пользователь', related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, verbose_name='пост', related_name='comments', on_delete=models.CASCADE)
    text = models.TextField('текст')
    created = models.DateTimeField('создана', auto_now_add=True)

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'

    def __str__(self):
        return 'комментарий от {} к посту {}'.format(self.user.get_full_name, self.post.title)
