from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from django.db.models import Count

from project.apps.blog.api.serializers import PostSerializer, PostShortSerializer
from project.apps.blog.models import Post


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all().annotate(likes_count=Count('likes',
                                                             distinct=True))
    serializers = {
        'default': PostSerializer,
        'list': PostShortSerializer
    }

    def get_serializer_class(self):
        return self.serializers.get(self.action,
                                    self.serializers['default'])

    @detail_route(methods=['get'])
    def likes(self, request, **kwargs):
        obj = self.get_object()
        response = {
            'likes_count': obj.get_likes_count()
        }
        return Response(response)
