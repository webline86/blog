from rest_framework import routers

from django.urls import path, include

from project.apps.blog.api.viewsets import PostViewSet

router = routers.DefaultRouter()
router.register('posts', PostViewSet)

app_name = 'blog'

urlpatterns = [
    path('', include(router.urls)),
]
