from rest_framework import serializers

from project.apps.blog.models import Post


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'id',
            'title',
            'text',
            'created'
        )

        read_only_fields = (
            'id',
        )


class PostShortSerializer(serializers.ModelSerializer):
    likes_count = serializers.IntegerField(source='get_likes_count')

    class Meta:
        model = Post
        fields = (
            'id',
            'title',
            'created',
            'likes_count'
        )

        read_only_fields = fields
