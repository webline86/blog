from django.contrib import admin

from .models import Post, Like, Comment


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'get_likes_count')
    search_fields = ('title', 'text')

    def get_likes_count(self, obj):
        return obj.get_likes_count()
    get_likes_count.short_description = 'количество лайков'


class LikeAdmin(admin.ModelAdmin):
    list_display = ('post', 'user')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'created', 'post')
    search_fields = ('user__username', 'post__title', 'text')


admin.site.register(Post, PostAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(Comment, CommentAdmin)
